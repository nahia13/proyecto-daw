﻿drop database proyecto;
create database proyecto;
use proyecto;

CREATE TABLE PRODUCTOS (id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  categoria varchar(50) NOT NULL,
  nombre varchar(50) NOT NULL,
  descripcion varchar(50) NOT NULL,
  precio decimal(7,2) NOT NULL,
  imagen varchar(250) NOT NULL,
  marca varchar(20) NOT NULL,
  destacado varchar(50) NOT NULL
 );

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES('Aromas', 'Aroma Albaricoque','Aroma Albaricoque',4.99,'./assets/aromas/aromaalbaricoque.jpg','Lorann Oils','FALSE');

INSERT INTO PRODUCTOS (categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Aromas', 'Aroma Algodón','Aroma Algodón',4.99,'./assets/aromas/aromaalgodon.jpg','Lorann Oils','TRUE'
);
INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Aromas', 'Aroma Champán','Aroma Champán',4.99,'./assets/aromas/aroma-champan.jpg','Lorann Oils','TRUE'
);
INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Aromas', 'Aroma de café','Aroma café',4.99,'./assets/aromas/aroma-concentrado-de-cafe.jpg','Lorann Oils','TRUE'
);
INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Aromas', 'Aroma Coco','Aroma Coco',4.99,'./assets/aromas/aroma-concentrado-de-coco.jpg','Lorann Gourmet','FALSE'
);

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Colorantes', 'Set 4 colorantes','Set 4 colorantes',4.99,'./assets/colorantes/set4colores.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Colorantes', 'Set 8 colorantes','Set 8 colorantes',12.99,'./assets/colorantes/set8colores.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Colorantes', 'Set 12 colorantes','Set 12 colorantes',24.99,'./assets/colorantes/set12colores.jpg','Lorann Gourmet','FALSE');


INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Cortadores', 'Cortador abeja','Cortador estrella',3.99,'./assets/cortadores/cortador-abeja-sonriente.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Cortadores', 'Cortador árbol','Cortador estrella',3.99,'./assets/cortadores/cortador-gigante-arbol-de-navidad.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Cortadores', 'Cortador estrella','Cortador estrella',3.99,'./assets/cortadores/cortador-gigante-estrella-fugaz.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Decoracion', 'Purpurina','Purpurina roja',2.99,'./assets/decoracion/purpurinaroja.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Decoracion', 'Icing blanco','Icing blanco',5.99,'./assets/decoracion/icing-blanco-280-gr.jpg','Lorann Gourmet','FALSE');


INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Fondant', 'Fondant azul','Fondant azul',7.99,'./assets/fondant/fondant-pastkolor-azul-1.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Fondant', 'Fondant blanco','Fondant blanco',7.99,'./assets/fondant/fondant-pastkolor-blanco.jpg','Lorann Gourmet','FALSE');


INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Moldes', 'Molde bavaria','Molde bavaria',17.99,'./assets/moldes/molde-bundt-bavaria.jpg','Lorann Gourmet','FALSE');

INSERT INTO PRODUCTOS(categoria,nombre,descripcion,precio,imagen,marca,destacado) VALUES(
'Moldes', 'Molde clasico','Molde clasico',17.99,'./assets/moldes/molde-bundt-clasico.jpg','Lorann Gourmet','FALSE');



CREATE TABLE USUARIOS(
id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
email varchar(100) NOT NULL,
pass varchar(100) NOT NULL,
nombre varchar(100) NOT NULL,
apellidos varchar(100) NOT NULL,
telefono varchar(100) NOT NULL,
direccion varchar(100) NOT NULL,
cp varchar(20) NOT NULL
);

CREATE TABLE PEDIDOS(    
  id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idUsuario int(10) NOT NULL,
  CONSTRAINT FK_PedidosUsuario FOREIGN KEY (idUsuario) REFERENCES USUARIOS(id)
);

CREATE TABLE DETALLEPEDIDOS(    
  id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idProducto int(10) NOT NULL,
  idPedido int(10) NOT NULL,
  cantidad varchar(50) NOT NULL,
  precio FLOAT(20) NOT NULL,
  CONSTRAINT FK_PedidosProductos FOREIGN KEY (idProducto) REFERENCES PRODUCTOS(id),
  CONSTRAINT FK_DetallePedidos FOREIGN KEY (idPedido) REFERENCES PEDIDOS(id)
 
);


