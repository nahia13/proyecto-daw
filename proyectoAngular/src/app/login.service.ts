import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from './app.settings';
import { Usuario } from './usuario.model';



const URL = AppSettings.URL;

@Injectable()
export class LoginService {

    @Output() isLoggedEvent: EventEmitter<Boolean> = new EventEmitter(true);

    constructor(private http: Http, private router: Router) { }

    login(username: string, password: string) {
        return this.http.post(URL + 'login.php', JSON.stringify({ email: username, pass: password }))
            .map((response: Response) => {
                console.log(response);

                // funciona login si hay jwt token en la respuesta
                let usuario = response.json();
                console.log(usuario);
                if (usuario) {
                    // guarda los detalles del usuario en el local storage entre paginas
                    localStorage.setItem('Usuario', JSON.stringify(usuario));

                }
                this.actualizarLogin();
            });

    }




    logout() {
        localStorage.removeItem('Usuario');
        this.actualizarLogin();
    }

    islogged() {
        return this.getUsuario() != undefined;
    }

    getUsuario() {
        let usuario: Usuario = JSON.parse(localStorage.getItem("Usuario"));
        return usuario;
    }
    getUsuarioId() {
        let usuario: Usuario = JSON.parse(localStorage.getItem("Usuario"));
        let idUsuario = usuario.id;
        return idUsuario;
    }

    actualizarLogin() {
        this.isLoggedEvent.emit(this.islogged());
    }


    private handleError(error: any) {
        console.error(error);
        return Observable.throw("Server error (" + error.status + "): " + error.text());
    }




}