import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {


  constructor(private loginService: LoginService, private router: Router) { }
  

  ngOnInit() {

  }

  login(username: string, password: string) {
    this.loginService.login(username, password).subscribe(
      response => {

        this.router.navigate(['']);
      },

      error => alert('Error en el usuario o contraseña')
    );

  }




}


