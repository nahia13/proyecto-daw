import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './app.settings';
import 'rxjs/Rx';

import { Usuario } from './usuario.model';

const URL = AppSettings.URL;

@Injectable()
export class UsuarioService {

    constructor(private http: Http) { }

    registrarDatos(usuario: Usuario) {
        let url = URL + 'registro.php';
        return this.http.post(url, usuario)
            .map(response => response.json());

    }
}