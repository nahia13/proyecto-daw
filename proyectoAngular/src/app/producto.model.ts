export interface Producto {
    id?: number;
    nombre: string;
    categoria: string;
    descripcion: string;
    precio: number;
    marca: string;
    imagen: string;

}