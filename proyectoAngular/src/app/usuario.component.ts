import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from './usuario.model';
import { UsuarioService } from './usuario.service';
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {
  formGroup: FormGroup;
  private email: string;
  private pass: string;
  private nombre: string;
  private apellidos: string;
  private telefono: string;
  private direccion: string;
  private cp: string;


  constructor(private usuarioService: UsuarioService) { }
  //lo que hace al inicializar la pagina
  ngOnInit() {
    this.formGroup = new FormGroup({
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      Nombre: new FormControl('', [
        Validators.required,
        Validators.maxLength(15)
      ]),
      Apellidos: new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      Telefono: new FormControl('', [
        Validators.required,
        Validators.maxLength(9),
        Validators.pattern(/^[679]{1}[0-9]{8}$/)

      ]),
      Direccion: new FormControl('', [
        Validators.required,
        Validators.maxLength(100)
      ]),
      CP: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern(/^[0-9]{5}$/),
        Validators.maxLength(5)
      ])

    });
  }

  onSubmit() {
    console.log(this.formGroup);
  }

  onReset() {
    this.formGroup.reset();
  }

  registrarDatos() {
    let usuario: Usuario = {
      email: this.email,
      pass: this.pass,
      nombre: this.nombre,
      apellidos: this.apellidos,
      telefono: this.telefono,
      direccion: this.direccion,
      cp: this.cp
    };



    this.usuarioService.registrarDatos(usuario).subscribe(
      data => this.emailRepetido(data),
      error => console.log(error)
    );
   
  }
  emailRepetido(data){
    if(!data){
      window.alert('Email ya está en uso');
    }else{
      window.alert('Usuario registrado correctamente');
      this.vaciarCampos();
    }
  }

  vaciarCampos(){
   this.email='';
   this.pass='';
   this.nombre='';
   this.apellidos='';
   this.telefono='';
   this.direccion='';
   this.cp='';
  }
}
