import { Component, OnInit } from '@angular/core';
import { Producto } from './producto.model';
import { Articulo } from './articulo.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductoService } from './producto.service';
import { CestaService } from './cesta.service';
@Component({
  selector: 'lista-productos',
  templateUrl: './lista-productos.component.html'
})
export class ListaProductosComponent implements OnInit {
  private productos: Producto[];
  private categoria: String;
  constructor(private activatedRoute: ActivatedRoute, private productoService: ProductoService, private cestaService: CestaService, private router: Router) {

    router.events.subscribe((val) => {
      this.categoria = this.activatedRoute.snapshot.params['categoria'];
    });
  }

  ngOnInit() {
    this.productos = new Array;
    this.cargarProductos();


  }
  addProducto(producto: Producto) {

    let articulo: Articulo = {
      id: producto.id,
      nombre: producto.nombre,
      precio: producto.precio,
      cantidad: 1
    }
    this.cestaService.addArticulo(articulo);

  }
  cargarProductos() {
    this.productoService.getProductos().subscribe(
      data => this.getData(data),

      error => console.log(error)
    );
  }

  getData(data) {
    this.productos = data;

  }

  setCategoria(cat) {
    this.categoria = cat;


  }

  showCategoria(cat: String) {

    return cat.toUpperCase() == this.categoria.toUpperCase();

  }

}
