import { Component, OnInit } from '@angular/core';
import { ProductoService } from './producto.service';
import { Producto } from './producto.model';
import { Articulo } from './articulo.model';
import { CestaService } from './cesta.service';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'detallep',
  templateUrl: './detallep.component.html'
})
export class DetallepComponent implements OnInit {

  private producto: Producto;
  private id: number;
  private cantidad: number;

  constructor(private activatedRoute: ActivatedRoute, private productoService: ProductoService, private cestaService: CestaService) {

  }

  ngOnInit() {

    this.id = this.activatedRoute.snapshot.params['id'];

    this.productoService.getDetalles(this.id).subscribe(
      data => this.producto = data,
      error => console.error(error)
    )



  }
  validarCantidad() {
    if (this.cantidad) {
      return true;
    }
    return false;
  }
  addProducto(producto: Producto) {
    if (this.validarCantidad()) {
      let articulo: Articulo = {
        id: producto.id,
        nombre: producto.nombre,
        precio: producto.precio,
        cantidad: this.cantidad
      }
      this.cestaService.addArticulo(articulo);

    }





  }
}

