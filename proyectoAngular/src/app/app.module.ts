import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ListaProductosComponent } from './lista-productos.component';
import { UsuarioComponent } from './usuario.component';
import { IndexComponent } from './index.component';
import { HeaderComponent } from './header.component';
import { FooterComponent } from './footer.component';
import { ContactarComponent } from './contactar.component';
import { DetallepComponent } from './detallep.component';
import { CestaComponent } from './cesta.component';
import { LoginComponent } from './login.component';
import { PoliticaComponent } from './politica.component';
import { NavbarComponent } from './navbar.component';
import { MapaWebComponent } from './mapaweb.component';



import { BreadcrumbComponent } from './breadcrumb.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';


import { ProductoService } from './producto.service';
import { UsuarioService } from './usuario.service';
import { LoginService } from './login.service';
import { CestaService } from './cesta.service';
import { EmailService } from './email.service';



import { routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    ListaProductosComponent,
    UsuarioComponent,
    IndexComponent,
    HeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    ContactarComponent,
    DetallepComponent,
    CestaComponent,
    LoginComponent,
    PoliticaComponent,
    NavbarComponent,
    MapaWebComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    routing,
    CarouselModule.forRoot(),
    NgbModule.forRoot(),
    ReactiveFormsModule,
    NgBootstrapFormValidationModule.forRoot()
  ],
  providers: [
    ProductoService,
    UsuarioService,
    LoginService,
    CestaService,
    EmailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
