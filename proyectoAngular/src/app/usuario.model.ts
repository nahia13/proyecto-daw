export interface Usuario {
    id?: number;
    email: string;
    pass: string;
    nombre: string;
    apellidos: string;
    telefono: string;
    direccion: string;
    cp: string|number;
}