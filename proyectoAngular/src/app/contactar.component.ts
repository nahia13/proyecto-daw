import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { EmailService, IMessage } from './email.service';

@Component({
  selector: 'contactar',
  templateUrl: './contactar.component.html'
})
export class ContactarComponent implements OnInit {
  formGroup: FormGroup;
  title = 'Angular PHP Email Example!';
  message: IMessage = {};
  private email: string;
  private name: string;
  private mensaje: string;

  constructor(private emailService: EmailService) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      Nombre: new FormControl('', [
        Validators.required,
        Validators.maxLength(15)
      ]),
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Mensaje: new FormControl('', [
        Validators.required
      ])

    });
  }
  sendEmail() {

    let message: IMessage = {
      email: this.email,
      name: this.name,
      message: this.mensaje
    }



    this.emailService.sendEmail(message).subscribe(res => {
      console.log('AppComponent Success', res);
      window.alert('Mensaje enviado');
      this.vaciarCampos();
    }, error => {
      console.log('AppComponent Error', error);
    })


  }
vaciarCampos(){
 this.email='';
 this.name='';
 this.mensaje='';
}

  onSubmit() {
    console.log(this.formGroup);
  }

  onReset() {
    this.formGroup.reset();
  }





}


