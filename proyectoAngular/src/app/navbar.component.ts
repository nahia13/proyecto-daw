import { Component, Output, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './login.service';

@Component({
    selector: 'ct-navbar',
    templateUrl: './navbar.component.html'
})

export class NavbarComponent implements OnInit {
    isIn = false;
    private logged: boolean;

    constructor(private loginService: LoginService) { }

    ngOnInit() {

        this.loginService.isLoggedEvent.subscribe(boolean => this.logged = boolean);
        this.loginService.actualizarLogin();

    }

    logout() {
        this.loginService.logout();
    }

    toggleState() {
        let bool = this.isIn;
        this.isIn = bool === false ? true : false;
    }



}


