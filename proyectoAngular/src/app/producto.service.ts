import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { AppSettings } from './app.settings';
import { Producto } from './producto.model';

const URL = AppSettings.URL;

@Injectable()
export class ProductoService {

	constructor(private http: Http) { }

	getProductos() {
		let url = URL + 'listarproductos.php';
		return this.http.get(url)
			//te transforma una respuesta json a una clase de angular
			.map(response => response.json())
			.catch(error => this.handleError(error));

	}

	getDestacados() {
		let url = URL + 'destacados.php';
		return this.http.get(url)
			.map(response => response.json())
			.catch(error => this.handleError(error));
	}


	getDetalles(id: number) {
		let url = URL + 'detalleproductos.php';
		return this.http.post(url, JSON.stringify({ id: id }))
			.map(response => response.json())
			.catch(error => this.handleError(error));
	}

	private handleError(error: any) {
		console.error(error);
		return Observable.throw("Server error (" + error.status + "): " + error.text())
	}

}