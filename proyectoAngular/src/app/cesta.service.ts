import { Injectable, Output, EventEmitter } from '@angular/core';
import { Articulo } from './articulo.model';
import { Http } from '@angular/http';
import { Pedido } from './pedido.model';
import { AppSettings } from './app.settings';
const URL = AppSettings.URL;

@Injectable()
export class CestaService {
    private cesta: Articulo[] = [];
    constructor(private http: Http) { }
    addArticulo(articulo: Articulo) {

        let encontrado: boolean = false;
        for (let art of this.cesta) {
            if (articulo.id == art.id) {
                art.cantidad = Number(art.cantidad) + Number(articulo.cantidad);
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            this.cesta.push(articulo);

        }
        console.log(this.cesta);
        this.guardarCesta();
    }



    limpiarCesta() {
        this.cesta = [];
        this.guardarCesta();
    }
    cargarCesta() {
        this.cesta = JSON.parse(localStorage.getItem('Cesta'));
        if(!this.cesta){
            this.cesta=[];
        }
    }
    getCesta(): Articulo[] {
        return this.cesta;

    }

    guardarCesta() {
        localStorage.setItem('Cesta', JSON.stringify(this.cesta));
        this.actualizarCesta();
    }
    getPrecioTotal() {
        let sumaTotal = 0;
        for (let art of this.cesta) {
            let precio = art.precio * art.cantidad;
            sumaTotal = sumaTotal + precio;

        }

        return sumaTotal.toFixed(2);


    }
    numeroArticulos() {
        let suma: number = 0;
        for (let articulo of this.cesta) {

            suma = suma + Number(articulo.cantidad);
        }

        return suma;
    }

    @Output() cestaChangeEvent: EventEmitter<Number> = new EventEmitter(true);

    actualizarCesta() {
        this.cestaChangeEvent.emit(this.numeroArticulos());

    }

    guardarPedido(pedido: Pedido) {
        let url = URL + 'insertarPedido.php';
        return this.http.post(url, pedido)
            .map(response => response.json());

    }



}
