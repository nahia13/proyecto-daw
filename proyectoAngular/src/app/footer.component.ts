import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'static-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {


  constructor() { }

  ngOnInit() {

  }

  goTop() {
    window.scrollTo(0, 0);
  }



}


