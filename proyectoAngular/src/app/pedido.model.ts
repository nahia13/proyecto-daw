import { Articulo } from './articulo.model';
export interface Pedido {
    id?: number;
    idUsuario: number;
    articulos: Articulo[];
}