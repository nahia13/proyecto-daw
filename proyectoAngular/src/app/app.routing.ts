import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { UsuarioComponent } from './usuario.component';
import { ListaProductosComponent } from './lista-productos.component';
import { IndexComponent } from './index.component';
import { ContactarComponent } from './contactar.component';
import { DetallepComponent } from './detallep.component';
import { CestaComponent } from './cesta.component';
import { LoginComponent } from './login.component';
import { PoliticaComponent } from './politica.component';
import { MapaWebComponent } from './mapaweb.component';


const routes: Routes = [
    { path: '', component: IndexComponent},
    { path: 'producto/:categoria', component: ListaProductosComponent, data: { breadcrumb: "Productos" } },
    { path: 'contactar', component: ContactarComponent, data: { breadcrumb: "Contactar" } },
    { path: 'detallep/:id', component: DetallepComponent, data: { breadcrumb: "Detalle del producto" } },
    { path: 'cesta', component: CestaComponent, data: { breadcrumb: "Cesta" } },
    { path: 'login', component: LoginComponent, data: { breadcrumb: "Login" } },
    { path: 'politica', component: PoliticaComponent, data: { breadcrumb: "Política" } },
    { path: 'registro', component: UsuarioComponent, data: { breadcrumb: "Registro" } },
    { path: 'mapaweb', component: MapaWebComponent, data: { breadcrumb: "Mapa web" } },
    { path: 'logout', redirectTo: '' },
    { path: '**', redirectTo: '' }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);