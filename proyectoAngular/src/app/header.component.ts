import { Component, Output, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CestaService } from './cesta.service';


@Component({
  selector: 'static-header',
  templateUrl: './header.component.html'
})


export class HeaderComponent implements OnInit {
  private articulos: number;


  constructor(private cestaService: CestaService) { }

  ngOnInit() {
    this.cestaService.cargarCesta();

    this.cestaService.cestaChangeEvent.subscribe(number => this.articulos = number);
    this.cestaService.actualizarCesta();


  }





}


