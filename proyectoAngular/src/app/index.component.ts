import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CarouselModule } from 'ngx-bootstrap';
import { ProductoService } from './producto.service';
import { CestaService } from './cesta.service';
import { Producto } from './producto.model';
import { Articulo } from './articulo.model';

@Component({
  selector: 'indice',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
  private productos: Producto[];
  private mobileScreen = false;
  private innerWidth;
  constructor(private productoService: ProductoService, private cestaService: CestaService) {
    this.innerWidth = window.screen.width;
    if(innerWidth<750){
    this.mobileScreen=true;
    console.log(innerWidth);
    }
    console.log('fueraif'+innerWidth);
  }

  ngOnInit() {
    this.productos = new Array;
    this.cargarProductos();

  }
  addProducto(producto: Producto) {
    
        let articulo: Articulo = {
          id: producto.id,
          nombre: producto.nombre,
          precio: producto.precio,
          cantidad: 1
        }
        this.cestaService.addArticulo(articulo);
    
      }

 cargarProductos() {
    this.productoService.getDestacados().subscribe(
      data => this.getData(data),

      error => console.log(error)
    );
  }
  getData(data) {
    this.productos = data;

  }
}
