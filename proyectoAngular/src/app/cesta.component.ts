import { Component, OnInit } from '@angular/core';
import { Articulo } from './articulo.model';
import { Pedido } from './pedido.model';
import { CestaService } from './cesta.service';
import { ProductoService } from './producto.service';
import { LoginService } from './login.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'cesta',
  templateUrl: './cesta.component.html'
})
export class CestaComponent implements OnInit {

  private cestaArticulos: Articulo[];
  private precioTotal: string;
  private logged:boolean;

  constructor(private cestaService: CestaService, private loginService: LoginService) {

  }

  ngOnInit() {
    this.cestaArticulos = this.cestaService.getCesta();
    this.actualizarTotal();
    this.logged=this.loginService.islogged();

  }

  vaciarCesta() {
    this.cestaService.limpiarCesta();
    this.cestaArticulos = this.cestaService.getCesta();
    this.actualizarTotal();
  }
  actualizarCantidad(nuevaCantidad, articulo: Articulo) {
    articulo.cantidad = nuevaCantidad;
    this.actualizarTotal();
    localStorage.setItem('Cesta', JSON.stringify(this.cestaArticulos));
  }

  actualizarTotal() {
    this.precioTotal = this.cestaService.getPrecioTotal();
  }

  borrar(articulo: Articulo) {
    this.actualizarTotal();
    for (var i = 0; i < this.cestaArticulos.length; i++) {
      if (articulo.id == this.cestaArticulos[i].id) {
        this.cestaArticulos.splice(i, 1);

      }

    }
    localStorage.setItem('Cesta', JSON.stringify(this.cestaArticulos));
    this.actualizarTotal();
    this.cestaService.actualizarCesta();
  }

  insertarPedido() {
       
      let pedido: Pedido = {
        idUsuario: this.loginService.getUsuarioId(),
        articulos: this.cestaArticulos
      };

      this.cestaService.guardarPedido(pedido).subscribe(
        error => console.log(error)
      );
  
      this.vaciarCesta();
    
      
    


  }
}
