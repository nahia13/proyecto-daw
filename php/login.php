<?php
require("acceso.php");
try{
$con=conectarDB();

$data = file_get_contents("php://input");
$objData = json_decode($data);

$email = $objData->email;

$pass = $objData->pass;

$stmt = $con->prepare("SELECT * FROM usuarios WHERE email=:email LIMIT 1"); 

$stmt->bindParam(":email", $email);
$stmt->execute();
$count=$stmt->rowCount();


$datos=$stmt->fetch(PDO::FETCH_ASSOC);

$con = null;

if($count>0)
{
 
    if(password_verify($pass, $datos['pass']))
    {
        echo json_encode($datos);
    }else{
        echo false;
    }

 }
else{
echo false;
} 
}catch(PDOException $e) {
echo '{"error":{"text":'. $e->getMessage() .'}}';
}


?>